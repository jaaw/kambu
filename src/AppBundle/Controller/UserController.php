<?php
/**
 * Created by PhpStorm.
 * User: Agnieszka
 * Date: 2017-07-10
 * Time: 13:39
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('ROLE_USER')")
 */
class UserController extends  Controller
{
    /**
     * @Route("/edit-user/{id}",name="edit_user")
     */
    public function editUserAction(Request $request,User $user)
    {

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var User $user */
            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();


            $this->addFlash(
                'success',
                $this->get('translator')->trans("user.edit.message",array(),'FOSUserBundle'));

        }
        return $this->render('user/edit_user.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/user-list",name="user_list")
     */
    public function userListAction()
    {
        $userRepo = $this->getDoctrine()->getRepository('AppBundle:User');
        $users = $userRepo->findAll();

        return $this->render('user/user_list.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * @Route("/delete-user/{id}",name="delete_user")
     */
    public function deleteUserAction(User $user)
    {
        if (!$user) {
            throw $this->createNotFoundException('No user found');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($user);
        $em->flush();
        $this->addFlash(
            'success',
            $this->get('translator')->trans("user.delete.message",array(),'FOSUserBundle'));

        return $this->redirect($this->generateUrl('user_list'));
    }

}