<?php
/**
 * Created by PhpStorm.
 * User: Agnieszka
 * Date: 2017-07-10
 * Time: 18:14
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Address;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setUsername('admin');
        $userAdmin->setPlainPassword('password');
        $userAdmin->setEnabled(true);
        $userAdmin->setEmail('admin@test.pl');
        $userAdmin->setFirstName('Jan');
        $userAdmin->setLastName('Kowalski');

        $address = new Address();
        $address->setStreet('Piękna 18');
        $address->setZipCode('10-300');
        $address->setCity('Kraków');

        $userAdmin->setAddress($address);

        $manager->persist($userAdmin);
        $manager->flush();
    }
}