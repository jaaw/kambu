<?php
/**
 * Created by PhpStorm.
 * User: Agnieszka
 * Date: 2017-07-10
 * Time: 11:08
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('street',null, array('label' => 'form.street', 'translation_domain' => 'FOSUserBundle'))
            ->add('zipCode', null,array('label' => 'form.zip_code', 'translation_domain' => 'FOSUserBundle'))
            ->add('city', null,array('label' => 'form.city', 'translation_domain' => 'FOSUserBundle'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Address',
        ));
    }


}