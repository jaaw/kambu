<?php

/**
 * Created by PhpStorm.
 * User: Agnieszka
 * Date: 2017-05-29
 * Time: 10:40
 */
namespace AppBundle\Form;

use AppBundle\AppBundle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName',null, array('label' => 'form.first_name', 'translation_domain' => 'FOSUserBundle'))
        ->add('lastName', null,array('label' => 'form.last_name', 'translation_domain' => 'FOSUserBundle'))
        ->add('address','AppBundle\Form\AddressType',array('label' => 'form.address', 'translation_domain' => 'FOSUserBundle'));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}