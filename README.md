Zadanie rekrutacyjne - Kambu
========================


How to install:
--------------
1 . Sklonować repozytorium

2 . Uruchomić composera przez komendę 

```
#!php

composer install
```
 
  i uzupełnić odpowiednie parametry do bazy danych.

3 . Uruchomić komendy tworzące i uzupełniające bazę danych

```
#!php

 ./bin/console doctrine:database:create
 ./bin/console doctrine:migrations:migrate
 ./bin/console doctrine:fixtures:load
```

4 . Do systemu można zalogować się na dane: 

```
#!php

login: admin
hasło: password
```

Demo online: http://dev.jaw.usermd.net/web